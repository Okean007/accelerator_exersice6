import 'package:flutter/material.dart';
import 'package:project1/constants/appAssets.dart';
import 'package:project1/constants/appColors.dart';
import 'package:project1/constants/appStyles.dart';
import 'package:project1/screen/screen_person.dart';
import 'package:project1/screen/widgets/text_field_log.dart';
import 'package:project1/screen/widgets/text_fiels_pass.dart';

import 'generated/l10n.dart';

const Map account = {'login': 'qwerty', 'password': '123456ab'};

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();
  final _loginController = TextEditingController();
  final _passController = TextEditingController();

  @override
  void dispose() {
    _loginController.dispose();
    _passController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        bottom: false,
        child: Column(
          children: [
            Expanded(
              child: Image.asset(
                AppAssets.images.logotip,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      S.of(context).login,
                      style: AppStyles.s16w400.copyWith(
                        height: 2.0,
                      ),
                    ),
                    LoginTextField(controller: _loginController),
                    Text(
                      S.of(context).password,
                      style: AppStyles.s16w400.copyWith(
                        height: 2.5,
                      ),
                    ),
                    PasswordTextField(controller: _passController),
                    const SizedBox(
                      height: 30,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          primary: AppColors.primary,
                          padding: const EdgeInsets.symmetric(
                            vertical: 15.0,
                          ),
                        ),
                        onPressed: () {
                          formKey.currentState!.validate();
                          if (account['login'] == _loginController.text &&
                              account['password'] == _passController.text) {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const PersonScreen(),
                              ),
                            );
                          } else {
                            _showMyDialog(context);
                          }
                        },
                        child: Text(S.of(context).signIn),
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          S.of(context).notAccount,
                          style: const TextStyle(
                            color: AppColors.neutral2,
                          ),
                        ),
                        TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            primary: AppColors.more1,
                            textStyle: AppStyles.s16w400,
                          ),
                          child: Text(
                            S.of(context).create,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Future<void> _showMyDialog(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        title: Text(S.of(context).error),
        content: Text(
          S.of(context).invalidpasswordorlogin,
          style: AppStyles.s16w400,
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: SizedBox(
              width: double.infinity,
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  textStyle: AppStyles.s16w400.copyWith(
                    color: AppColors.primary,
                  ),
                  side: const BorderSide(
                    color: AppColors.primary,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  padding: const EdgeInsets.all(12),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  S.of(context).ok,
                ),
              ),
            ),
          ),
        ],
      );
    },
  );
}
