import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:project1/constants/appColors.dart';

import 'package:project1/l10n/app_locale.dart';
import 'package:project1/screen/splashScreen.dart';
import 'generated/l10n.dart';

// import 'package:project1/login_screen.dart';
void main() {
  SystemChrome.setSystemUIOverlayStyle(
     const SystemUiOverlayStyle(
      statusBarColor: AppColors.splashBackground,
      statusBarIconBrightness: Brightness.light,
      systemNavigationBarColor: AppColors.splashBackground,
      systemNavigationBarIconBrightness: Brightness.light,
    ),
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final locale = AppLocale();
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
      locale: locale.getDefaultLocale(),
      home: const SplashScreen(),
    );
  }
}
