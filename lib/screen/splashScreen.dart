import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:project1/constants/appAssets.dart';
import 'package:project1/login_screen.dart';

void main() {}

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void dispose() {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: Colors.white70,
        statusBarIconBrightness: Brightness.dark,
        systemNavigationBarColor: Colors.white,
        systemNavigationBarIconBrightness: Brightness.light,
      ),
    );
    super.dispose();
  }

  @override
  void initState() {
    Future.delayed(
      const Duration(seconds: 2),
      () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(
            builder: (context) => const LoginScreen(),
          ),
        );
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      // top: false,
      child: Stack(
        children: [
          Positioned.fill(
            child: Image.asset(
              AppAssets.images.background,
            ),
          ),
          Positioned(
            top: 10,
            left: 40,
            child: Column(
              children: [
                SizedBox(
                  child: Image.asset(
                    AppAssets.images.logotip,
                  ),
                ),
                SizedBox(
                  child: Image.asset(
                    AppAssets.images.morty,
                  ),
                ),
                SizedBox(
                  child: Image.asset(
                    AppAssets.images.rick,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
