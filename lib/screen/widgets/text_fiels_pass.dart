import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../constants/appAssets.dart';
import '../../constants/appColors.dart';
import '../../generated/l10n.dart';

class PasswordTextField extends StatelessWidget {
  const PasswordTextField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        prefixIcon: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: SvgPicture.asset(
            AppAssets.svg.password,
            width: 25.0,
            color: AppColors.neutral2,
          ),
        ),
        hintText: S.of(context).password,
        border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            borderSide: BorderSide.none
            ),
        fillColor: AppColors.neutral1,
        filled: true,
        counterText: '',
      ),
      maxLength: 16,
      controller: controller,
      obscureText: true,
      validator: (password) {
        if (password == null || password.length < 8) {
          return S.of(context).inputErrorPasswordIsShort;
        }
        return null;
      },
    );
  }
}
