import 'package:flutter/material.dart';
import 'package:project1/constants/appAssets.dart';
import 'package:project1/constants/appStyles.dart';
import 'package:project1/constants/class_person.dart';

import '../../constants/appColors.dart';

class PersonListView extends StatelessWidget {
  const PersonListView({Key? key, required this.personList}) : super(key: key);
  final List<Person> personList;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: personList.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: [
              Image.asset(
                AppAssets.images.noAvatar,
                width: 100,
                height: 100,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    personList[index].status ?? '',
                    style: AppStyles.s16w400.copyWith(
                      color: _getStatusStyle(personList[index].status ?? ''),
                    ),
                  ),
                  Text(personList[index].name ?? '', style: AppStyles.s20w500),
                  Text(
                      '${personList[index].gender}, ${personList[index].species}'),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  _getStatusStyle(String status) {
    switch (status) {
      case 'Мертвый':
        return AppColors.more2;
      case 'Живой':
        return AppColors.more1;
    }
  }
}
