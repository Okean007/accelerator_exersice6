import 'package:flutter/material.dart';
import 'package:project1/constants/appAssets.dart';
import 'package:project1/constants/appColors.dart';
import 'package:project1/constants/appStyles.dart';
import 'package:project1/constants/class_person.dart';

class PersonGridView extends StatelessWidget {
  const PersonGridView({Key? key, required this.personList}) : super(key: key);
  final List<Person> personList;
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        mainAxisSpacing: 20.0,
        crossAxisSpacing: 8.0,
        childAspectRatio: 0.8,
        crossAxisCount: 2,
      ),
      itemCount: personList.length,
      itemBuilder: (BuildContext context, int index) {
        // ignore: avoid_unnecessary_containers
        return Container(
          child: Column(
            children: [
              Image.asset(
                AppAssets.images.noAvatar,
                width: 150,
                height: 150,
              ),
              Text(
                personList[index].status ?? '',
                style: AppStyles.s16w400.copyWith(
                    color: _getStatusStyle(personList[index].status ?? '')
                    ),
              ),
              Text(
                personList[index].name ?? '', style: AppStyles.s20w500
              ),
              Text(
                '${personList[index].gender}, ${personList[index].species}'
              ),
            ],
          ),
        );
      },
    );
  }

  _getStatusStyle(String status) {
    switch (status) {
      case 'Мертвый':
        return AppColors.more2;
      case 'Живой':
        return AppColors.more1;
    }
  }
}
