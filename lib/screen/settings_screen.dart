import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project1/constants/appColors.dart';
import 'package:project1/constants/appStyles.dart';
import 'package:project1/generated/l10n.dart';
import 'package:project1/l10n/app_locale.dart';
import 'package:project1/screen/widgets/navigat_bottom.dart';
class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    final locale = AppLocale();
    return Scaffold(
      bottomNavigationBar: const NavigationBottom(counter: 1),
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          S.of(context).setting,
          style: AppStyles.s20w500,
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        foregroundColor: AppColors.mainText,
        elevation: 0.0,
      ),
      body: Column(
       mainAxisAlignment: MainAxisAlignment.center,
       children: [
         Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('${S.of(context).language}: '
              ),
              DropdownButton(
                  value: locale.getLocale(Intl.getCurrentLocale()
                  ),
                  items: [
                    DropdownMenuItem(
                        value: locale.getLocale('en'),
                        child: Text(S
                            .of(context)
                            .english)
                    ),
                    DropdownMenuItem(
                        value: locale.getLocale('ru_RU'),
                        child: Text(S
                            .of(context)
                            .russian)
                    ),
                  ],
                  onChanged: <Locale>(value) async {
                    S.load(value);
                    setState(() {});
                  }
              ),
            ],
          ),
       ],
     ),
    );
  }
}

