import 'package:flutter/material.dart';
import 'package:project1/constants/appColors.dart';
import 'package:project1/constants/appStyles.dart';
import 'package:project1/constants/class_person.dart';
import 'package:project1/generated/l10n.dart';
import 'package:project1/screen/widgets/grid_view.dart';
import 'package:project1/screen/widgets/list_view.dart';
import 'package:project1/screen/widgets/search_widget.dart';

import 'widgets/navigat_bottom.dart';

class PersonScreen extends StatefulWidget {
  const PersonScreen({Key? key}) : super(key: key);
  @override
  State<PersonScreen> createState() => _PersonState();
}
class _PersonState extends State<PersonScreen> {
  bool isListView = true;
  @override
  Widget build(BuildContext context) {
    List<Person> personList = getPersonList();
    return Scaffold(
      bottomNavigationBar: const NavigationBottom(counter: 0),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SearchField(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  '${S.of(context).personsTotal} ${personList.length}',
                  style: AppStyles.s16w500.copyWith(
                    color: AppColors.neutral2,
                  ),
                ),
                IconButton(
                  icon: Icon(isListView ? Icons.list : Icons.grid_view),
                  iconSize: 32,
                  color: AppColors.neutral2,
                  onPressed: () {
                    setState(() {
                      isListView = !isListView;
                    });
                  },
                )
              ],
            ),
            const SizedBox(height: 25),
            Expanded(
              // child:
              child: isListView
                  ? PersonListView(personList: personList)
                  : PersonGridView(
                      personList: personList,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}

List<Person> getPersonList() {
  List<Person> personList = [];
  personList.add(
    const Person(
      name: 'Рик Санчез',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Живой',
    ),
  );
  personList.add(
    const Person(
      name: 'Алан Райс',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Живой',
    ),
  );
  personList.add(
    const Person(
      name: 'Саммер Смит',
      species: 'Человек',
      gender: 'Женский',
      status: 'Живой',
    ),
  );
  personList.add(
    const Person(
      name: 'Морти Смит',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Живой',
    ),
  );
  personList.add(
    const Person(
      name: 'Рик Санчез',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Мертвый',
    ),
  );
  personList.add(
    const Person(
      name: 'Рик Санчез',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Мертвый',
    ),
  );
  personList.add(
    const Person(
      name: 'Рик Санчез',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Мертвый',
    ),
  );
  personList.add(
    const Person(
      name: 'Рик Санчез',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Мертвый',
    ),
  );
  personList.add(
    const Person(
      name: 'Рик Санчез',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Мертвый',
    ),
  );
  personList.add(
    const Person(
      name: 'Рик Санчез',
      species: 'Человек',
      gender: 'Мужской',
      status: 'Мертвый',
    ),
  );
  return personList;
}



